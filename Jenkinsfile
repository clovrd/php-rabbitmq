#!groovy

def label = "buildpod.${env.JOB_NAME}.${env.BUILD_NUMBER}".replace('-', '_').replace('/', '_')

def notifyBuild(String buildStatus = 'STARTED') {
  buildStatus =  buildStatus ?: 'SUCCESSFUL'

  def colorName = 'RED'
  def colorCode = '#ed5565'
  def subject = "${env.JOB_NAME} #${env.BUILD_NUMBER} ${buildStatus}"
  def summary = "${subject} Tag: ${GERRIT_CHANGE_ID}"

  if (buildStatus == 'STARTED') {
    color = 'YELLOW'
    colorCode = '#f8ac59'
  } else if (buildStatus == 'SUCCESSFUL') {
    color = 'GREEN'
    colorCode = '#1ab394'
  } else {
    color = 'RED'
    colorCode = '#ed5565'
  }

  slackSend (color: colorCode , message: summary, token: "${env.SLACK_TOKEN}")
}

podTemplate(label: label,
    containers: [
        containerTemplate(name: 'php', image: 'eu.gcr.io/delinero-microservices/php7_baseimage', ttyEnabled: true, command: 'cat'),
        containerTemplate(name: 'cloud-sdk', image: 'google/cloud-sdk', ttyEnabled: true, command: 'cat'),
        containerTemplate(name: 'jnlp', image: 'gcr.io/cloud-solutions-images/jenkins-k8s-slave', args: '${computer.jnlpmac} ${computer.name}', ttyEnabled: false)
    ],
    volumes: [
        hostPathVolume(mountPath: '/usr/bin/docker', hostPath: '/usr/bin/docker'),
        hostPathVolume(mountPath: '/var/run/docker.sock', hostPath: '/var/run/docker.sock'),
        secretVolume(secretName: 'jenkins-service', mountPath: '/etc/jenkinskeys')
    ]
)

{
    node(label) {
        try {
            notifyBuild('STARTED')

            stage('Checkout Git') {
                checkout scm
            }
            if ("${GERRIT_EVENT_TYPE}" == 'patchset-created') {
                stage('Test') {
                    container(name: 'php') {
                        sh 'composer install'
                        sh 'vendor/bin/phpunit'
                    }
                }
            }
        }
        catch (e) {
            currentBuild.result = "FAILED"
            throw e
        }
        finally {
          notifyBuild(currentBuild.result)
          setGerritReview customUrl: 'https://gerrit.delinero.de'
        }
    }
}