<?php

namespace AppBundle\Command;

use PhpAmqpLib\Message\AMQPMessage;
use PhpRabbitMq\BaseConsumer;
use PhpRabbitMq\PubSubConsumer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ConsumerCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('your-service:consumer:consumer-stuff')
            ->setDescription('testing');
    }
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $consumer = new PubSubConsumer(
            $container->get("rabbitmq.connection_factory")->getConnection(),
            "test",
            "test",
            $container->getParameter("kernel.environment"),
            "test",
            "test-listener",
            $container->get("logger")
        );
        $consumer->setCallback($this->getCallback());
        $consumer->consume(0);
    }

    protected function getCallback()
    {
        return function(AMQPMessage $msg) {

            $logger = $this->getContainer()->get("logger");
            $logger->notice($msg->getBody());
        };

    }


}
