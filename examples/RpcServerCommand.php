<?php

namespace AppBundle\Command;

use Monolog\Logger;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RpcServerCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('some-service:rpc-server:do-stuff')
            ->setDescription('a very niffty description');
    }
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $server = new \PhpRabbitMq\BaseRpcServer(
            $container->get("rabbitmq.connection_factory")->getConnection(),
            $container->get("rabbitmq.factory"),
            "testing",
            "testing",
            $container->getParameter("kernel.environment"),
            "testing",
            "rpc-server",
            $container->get("logger")
        );
        $server->setCallback($this->getCallback());
        $server->start();
    }

    protected function getCallback()
    {
        return function($msg) {
            $logger = $this->getContainer()->get("logger");
            $logger->notice($msg);
            return "server received " . $msg;
        };

    }

}
