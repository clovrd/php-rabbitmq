<?php
/**
 * @author Levin Mauritz <l.mauritz@delinero.de>
 *
 */

namespace PhpRabbitMq;

use PhpAmqpLib\Message\AMQPMessage;
use Thumper\RpcClient as BaseRpcClient;

class RpcClient extends BaseRpcClient
{
    /**
     * Add request to be sent to RPC Server.
     *
     * @param string $messageBody
     * @param string $server
     * @param string $requestId
     * @param string $routingKey
     */
    public function addRequest($messageBody, $server, $requestId, $routingKey = '')
    {
        if (empty($requestId)) {
            throw new \InvalidArgumentException("You must provide a request ID");
        }
        $this->setParameter('correlation_id', $requestId);
        $this->setParameter('reply_to', $this->queueName);

        $message = new AMQPMessage(
            $messageBody,
            $this->getParameters()
        );

        $this->channel
            ->basic_publish($message, $server, $routingKey);

        $this->requests++;
    }

}