<?php
/**
 * @author Levin Mauritz <l.mauritz@delinero.de>
 *
 */

namespace PhpRabbitMq;


interface ConnectionFactoryInterface
{
    public function getConnection();
}