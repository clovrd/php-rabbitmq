<?php
/**
 * @author Levin Mauritz <l.mauritz@delinero.de>
 *
 */

namespace PhpRabbitMq;

use PhpAmqpLib\Connection\AMQPStreamConnection;

interface RabbitMqFactoryInterface
{
    public function createProducer(AMQPStreamConnection $connection);

    public function createRpcClient(AMQPStreamConnection $connection);

    public function createRpcServer(
        AMQPStreamConnection $connection,
        string $exchange,
        string $queue,
        string $routingKey
    );

}