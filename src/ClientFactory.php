<?php
/**
 * @author Levin Mauritz <l.mauritz@delinero.de>
 *
 */

namespace PhpRabbitMq;


class ClientFactory
{
    public static function build($host,$port,$user,$password) {
        return new Client(
            new RabbitMqConnectionFactory($host,$port,$user,$password),
            new RabbitMqFactory()
        );
    }
}