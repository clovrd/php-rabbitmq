<?php
/**
 * @author Levin Mauritz <l.mauritz@delinero.de>
 *
 */

namespace PhpRabbitMq;


use PhpAmqpLib\Connection\AMQPLazyConnection;

class RabbitMqConnectionFactory implements ConnectionFactoryInterface
{
    /** @var  string */
    private $host;

    /** @var  string */
    private $port;

    /** @var  string */
    private $user;

    /** @var  string */
    private $password;

    /**
     * RabbitMqConnectionFactory constructor.
     * @param string $host
     * @param string $port
     * @param string $user
     * @param string $password
     */
    public function __construct($host, $port, $user, $password)
    {
        $this->host = $host;
        $this->port = $port;
        $this->user = $user;
        $this->password = $password;
    }

    public function getConnection(
        $connection_timeout = 3600.0,
        $read_write_timeout = 3600.0,
        $heartbeat = 1800
    ) {
        return new AMQPLazyConnection(
            $this->host,
            $this->port,
            $this->user,
            $this->password,
            $vhost = '/',
            $insist = false,
            $login_method = 'AMQPLAIN',
            $login_response = null,
            $locale = 'en_US',
            $connection_timeout,
            $read_write_timeout,
            $context = null,
            $keepalive = false,
            $heartbeat
        );
    }


}