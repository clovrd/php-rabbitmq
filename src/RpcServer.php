<?php
/**
 * @author Levin Mauritz <l.mauritz@delinero.de>
 *
 */

namespace PhpRabbitMq;

use Thumper\RpcServer as BaseRpcServer;

class RpcServer extends BaseRpcServer
{
    /**
     * Initialize Server.
     *
     * @param string $name Server name
     */
    public function initServer($name)
    {
        $this->setExchangeOptions(
            array('name' => $name, 'type' => 'direct')
        );
        $this->setQueueOptions(array('name' => $name));
    }

    /**
     * @return array
     */
    public function getExchangeOptions()
    {
        return $this->exchangeOptions;
    }

    /**
     * @return array
     */
    public function getQueueOptions()
    {
        return $this->queueOptions;
    }

}