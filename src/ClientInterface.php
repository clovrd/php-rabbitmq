<?php
/**
 * @author Levin Mauritz <l.mauritz@delinero.de>
 *
 */

namespace PhpRabbitMq;


interface ClientInterface
{
    public function publish($data, string $routingKey, string $exchange = '');

    public function rpcCall($data, string $routingKey, string $exchange = '', int $timeout = 30);

    public function getConnection();

}