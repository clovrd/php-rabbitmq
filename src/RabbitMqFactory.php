<?php
/**
 * @author Levin Mauritz <l.mauritz@delinero.de>
 *
 */

namespace PhpRabbitMq;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use Thumper\Producer;
use PhpRabbitMq\RpcClient;
use PhpRabbitMq\RpcServer;

class RabbitMqFactory implements RabbitMqFactoryInterface
{

    public function createProducer(AMQPStreamConnection $connection)
    {
        return new Producer($connection);
    }

    public function createRpcClient(AMQPStreamConnection $connection)
    {
        return new RpcClient($connection);
    }

    public function createRpcServer(
        AMQPStreamConnection $connection,
        string $exchange,
        string $queue,
        string $routingKey
    ) {
        $server = new RpcServer($connection);
        $server->setExchangeOptions([
            'name' => $exchange,
            'type' => 'topic'
        ]);
        $server->setQueueOptions(['name' => $queue ]);
        $server->setRoutingKey($routingKey);
        return $server;
    }
}