<?php
/**
 * @author Levin Mauritz <l.mauritz@delinero.de>
 *
 */

namespace PhpRabbitMq;

use PhpRabbitMq\RabbitMqFactoryInterface;
use const PHP_EOL;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Channel\AMQPChannel;
use Psr\Log\LoggerInterface;
use PhpRabbitMq\RpcServer;

class BaseRpcServer
{
    /** @var AMQPStreamConnection */
    private $connection;

    /** @var AMQPChannel */
    private $channel;

    /** @var string */
    private $exchange;

    /** @var string */
    private $queue;

    /** @var string */
    private $routingKey;

    /** @var  string */
    private $env;

    /** @var string */
    private $consumerName;

    /** @var LoggerInterface */
    private $logger;

    /** @var RpcServer  */
    private $server;

    public function __construct(
        AMQPStreamConnection $connection,
        RabbitMqFactoryInterface $rabbitMqFactory,
        string $exchange,
        string $routingKey,
        string $env,
        string $queue = null,
        string $consumerName = "PHP-CONSUMER",
        LoggerInterface $logger = null
    )
    {
        $this->connection = $connection;
        $this->channel = $this->connection->channel();
        $this->exchange = $exchange;
        $this->routingKey = $routingKey;
        $this->env = $env;
        $this->queue = $queue;
        $this->consumerName = $consumerName;
        $this->logger = $logger;

        $this->server = $rabbitMqFactory->createRpcServer(
            $this->connection,
            $this->getExchange(),
            $this->getQueue(),
            $this->getRoutingKey()
        );
    }

    public function start()
    {
        $this->logStart();
        $this->server->start();
    }

    public function setCallback(Callable $callable)
    {
        $this->server->setCallback($callable);
    }

    /**
     * @return string
     */
    public function getExchange()
    {
        return $this->exchange;
    }

    /**
     * @return string
     */
    public function getQueue()
    {
        return $this->queue;
    }

    /**
     * @return string
     */
    public function getRoutingKey()
    {
        return $this->routingKey;
    }

    /**
     * @return string
     */
    public function getEnv()
    {
        return $this->env;
    }

    /**
     * @return string
     */
    public function getConsumerName()
    {
        return $this->consumerName;
    }

    private function logStart()
    {
        if (!$this->logger) {
            return;
        }

        $this->logger->notice(PHP_EOL .
            "[*] running rpcServer " . $this->getConsumerName() . PHP_EOL .
            "- environment: \t" . $this->getEnv() . PHP_EOL .
            "- exchange: \t" . $this->getExchange() . PHP_EOL .
            "- routingKey: \t" . $this->getRoutingKey() . PHP_EOL .
            "- queue: \t" . $this->getQueue() . PHP_EOL . PHP_EOL
        );
    }

}