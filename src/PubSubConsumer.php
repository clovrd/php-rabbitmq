<?php
/**
 * @author Levin Mauritz <l.mauritz@delinero.de>
 *
 */

namespace PhpRabbitMq;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;
use Thumper\Consumer;

class PubSubConsumer extends Consumer
{
    /** @var string */
    private $exchange;

    /** @var string */
    private $queue;

    /** @var  string */
    private $env;

    /** @var string */
    private $consumerName;

    /** @var LoggerInterface */
    private $logger;

    public function __construct(
        AMQPStreamConnection $connection,
        string $exchange,
        string $routingKey,
        string $env,
        string $queue = null,
        string $consumerName = "PHP-CONSUMER",
        LoggerInterface $logger = null
    )
    {

        parent::__construct($connection);

        $this->exchange = $exchange;
        $this->env = $env;
        $this->queue = $queue;
        $this->consumerName = $consumerName;
        $this->logger = $logger;

        $this->setExchangeOptions([
            "name" => $exchange,
            "type" => "topic"
        ]);
        $this->setQueueOptions([
            "name" => $queue
        ]);

        $this->setRoutingKey($routingKey);
    }

    public function consume($numOfMessages)
    {
        $this->logStart();

        parent::consume($numOfMessages);
    }

    public function getChannel() {
        return $this->channel;
    }

    public function getConsumerTag()
    {
        return $this->getConsumerName();
    }

    public function processMessage(AMQPMessage $message)
    {
        call_user_func($this->callback, $message);
        $message->delivery_info['channel']
            ->basic_ack($message->delivery_info['delivery_tag']);
        $this->consumed++;
        $this->maybeStopConsumer($message);
    }

    /**
     * @return string
     */
    public function getExchange()
    {
        return $this->exchange;
    }

    /**
     * @return string
     */
    public function getQueue()
    {
        return $this->queue;
    }

    /**
     * @return string
     */
    public function getRoutingKey()
    {
        return $this->routingKey;
    }

    /**
     * @return string
     */
    public function getEnv()
    {
        return $this->env;
    }

    /**
     * @return string
     */
    public function getConsumerName()
    {
        return $this->consumerName;
    }

    private function logStart()
    {
        if (!$this->logger) {
            return;
        }

        $this->logger->notice(PHP_EOL .
            "[*] running " . $this->getConsumerName() . PHP_EOL .
            "- environment: \t" . $this->getEnv() . PHP_EOL .
            "- exchange: \t" . $this->getExchange() . PHP_EOL .
            "- routingKey: \t" . $this->getRoutingKey() . PHP_EOL .
            "- queue: \t" . $this->getQueue() . PHP_EOL . PHP_EOL
        );
    }

}