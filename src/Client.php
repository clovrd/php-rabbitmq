<?php
/**
 * @author Levin Mauritz <l.mauritz@delinero.de>
 *
 */

namespace PhpRabbitMq;


use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Exception\AMQPTimeoutException;
use PhpRabbitMq\Exception\RpcTimeoutException;
use Thumper\Producer;

class Client implements ClientInterface
{
    /** @var  AMQPStreamConnection */
    private $connection;

    /** @var  Producer */
    private $producer;

    /** @var  RpcClient */
    private $rpcClient;

    /**
     * RabbitService constructor.
     * @param ConnectionFactoryInterface $rabbitMqConnectionFactory
     * @param RabbitMqFactoryInterface $rabbitMqFactory
     */
    public function __construct(
        ConnectionFactoryInterface $rabbitMqConnectionFactory,
        RabbitMqFactoryInterface $rabbitMqFactory
    )
    {
        $this->connection = $rabbitMqConnectionFactory->getConnection();
        $this->producer = $rabbitMqFactory->createProducer($this->connection);
        $this->rpcClient = $rabbitMqFactory->createRpcClient($this->connection);
    }

    /**
     * @param mixed $data
     * @param string $routingKey
     * @param string $exchange
     */
    public function publish($data, string $routingKey, string $exchange = '')
    {
        $this->producer->setExchangeOptions(['name' => $exchange, 'type' => 'topic']);
        $this->producer->setRoutingKey($routingKey);
        $this->producer->publish($data);
    }

    /**
     * @param $data
     * @param string $routingKey
     * @param string $exchange
     * @param int $timeout
     * @return mixed
     * @throws RpcTimeoutException
     */
    public function rpcCall($data, string $routingKey, string $exchange = '', int $timeout = 30)
    {
        try {
            $this->rpcClient->initClient();
            $this->rpcClient->setTimeout($timeout);
            $requestId = uniqid();
            $this->rpcClient->addRequest($data, $exchange, $requestId, $routingKey);
            $replies = $this->rpcClient->getReplies();

            return $replies[$requestId];

        } catch (AMQPTimeoutException $e) {
            throw new RpcTimeoutException($routingKey,$exchange,$timeout);
        }
    }

  /**
   * @return AMQPStreamConnection
   */
    public function getConnection() {
      return $this->connection;
    }


}