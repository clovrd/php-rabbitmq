<?php
/**
 * @author Levin Mauritz <l.mauritz@delinero.de>
 *
 */

namespace PhpRabbitMq\Exception;


use Throwable;

class RpcTimeoutException extends \Exception
{

    public function __construct($routingKey, $exchange, $timeout)
    {
        parent::__construct("RPC to " . $routingKey . " of exchange " . $exchange . " timed out after " . $timeout . " seconds");
    }


}