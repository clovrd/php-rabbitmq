<?php
/**
 * @author Levin Mauritz <l.mauritz@posteo.de>
 *
 */

namespace PhpRabbitMq;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PHPUnit\Framework\TestCase;
use function socket_close;

class RabbitMqConnectionFactoryTest extends TestCase
{
    public $socket;

    public function tearDown()
    {
        parent::tearDown();
        socket_close($this->socket);
    }

    public function testGetConnection()
    {
        $this->socket = socket_create_listen(0);
        $connectionFactory = new RabbitMqConnectionFactory("test", "0", "test", "test");
        $connection = $connectionFactory->getConnection();
        $this->assertInstanceOf(AMQPStreamConnection::class, $connection);
    }

}
