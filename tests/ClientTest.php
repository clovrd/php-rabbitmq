<?php
/**
 * @author Levin Mauritz <l.mauritz@delinero.de>
 *
 */

namespace Test\PhpRabbitMq;

use Mockery;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Exception\AMQPTimeoutException;
use PhpRabbitMq\Client;
use PhpRabbitMq\ClientInterface;
use PhpRabbitMq\ConnectionFactoryInterface;
use PhpRabbitMq\Exception\RpcTimeoutException;
use PhpRabbitMq\RabbitMqFactory;
use PhpRabbitMq\RpcClient;
use PHPUnit\Framework\TestCase;
use Thumper\Producer;

include_once __DIR__ . '/overwrite.php';

class ClientTest extends TestCase
{

    public function testClientRpcCall()
    {
        $producerMock = Mockery::mock(Producer::class);

        $rpcClientMock = Mockery::mock(RpcClient::class);
        $rpcClientMock->shouldReceive("initClient", "setTimeout", "addRequest");
        $rpcClientMock
            ->shouldReceive("getReplies")
            ->andReturn(["yes, im a mock"]);

        $rabbitMqFactory = Mockery::mock(RabbitMqFactory::class);
        $rabbitMqFactory->shouldReceive("createProducer")->andReturn($producerMock);
        $rabbitMqFactory->shouldReceive("createRpcClient")->andReturn($rpcClientMock);

        $client = new Client($this->getConnectionFactoryMock(), $rabbitMqFactory);
        $output = $client->rpcCall("yolo", "foo", "bar");
        $this->assertEquals("yes, im a mock", $output);
    }

    public function testPublish()
    {
        $data = "data";
        $routingKey = "foo";
        $exchange = "bar";

        $producerMock = Mockery::mock(Producer::class);
        $producerMock
            ->shouldReceive("setExchangeOptions")
            ->with(['name' => $exchange, 'type' => 'topic']);
        $producerMock
            ->shouldReceive("setRoutingKey")
            ->with($routingKey);
        $producerMock
            ->shouldReceive("publish")
            ->with($data);

        $rpcClientMock = Mockery::mock(RpcClient::class);

        $rabbitMqFactory = Mockery::mock(RabbitMqFactory::class);
        $rabbitMqFactory->shouldReceive("createProducer")->andReturn($producerMock);
        $rabbitMqFactory->shouldReceive("createRpcClient")->andReturn($rpcClientMock);

        $client = new Client($this->getConnectionFactoryMock(), $rabbitMqFactory);
        $client->publish($data,"foo","bar");

        $this->assertInstanceOf(ClientInterface::class,$client);
    }

    public function testItShouldCatchAMQTimeoutExceptionAndThrowRpcTimeout() {

        $producerMock = Mockery::mock(Producer::class);

        $rpcClientMock = Mockery::mock(RpcClient::class);
        $rpcClientMock->shouldReceive("initClient", "setTimeout", "addRequest");
        $rpcClientMock
            ->shouldReceive("getReplies")
            ->andThrow(AMQPTimeoutException::class);

        $rabbitMqFactory = Mockery::mock(RabbitMqFactory::class);
        $rabbitMqFactory->shouldReceive("createProducer")->andReturn($producerMock);
        $rabbitMqFactory->shouldReceive("createRpcClient")->andReturn($rpcClientMock);

        $client = new Client($this->getConnectionFactoryMock(), $rabbitMqFactory);
        $this->expectException(RpcTimeoutException::class);
        $output = $client->rpcCall("yolo", "foo", "bar");
    }

    private function getConnectionFactoryMock(){
        $connectionFactoryMock = Mockery::mock(ConnectionFactoryInterface::class);
        $connectionFactoryMock
            ->shouldReceive("getConnection")
            ->andReturn((Mockery::mock(AMQPStreamConnection::class)));
        return $connectionFactoryMock;
    }

}
