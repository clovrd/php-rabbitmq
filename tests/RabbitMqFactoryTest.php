<?php
/**
 * @author Levin Mauritz <l.mauritz@delinero.de>
 *
 */

namespace PhpRabbitMq;

use Mockery;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PHPUnit\Framework\TestCase;
use Thumper\Producer;

class RabbitMqFactoryTest extends TestCase
{

    public function testCreateProducer() {
        $channel = Mockery::mock(AMQPChannel::class);
        $connection = Mockery::mock(AMQPStreamConnection::class);
        $connection->shouldReceive("channel")->andReturn($channel);

        $factory = new RabbitMqFactory();
        $producer = $factory->createProducer($connection);
        $this->assertInstanceOf(Producer::class,$producer);
    }

    public function testCreateRpcClient() {
        $channel = Mockery::mock(AMQPChannel::class);
        $connection = Mockery::mock(AMQPStreamConnection::class);
        $connection->shouldReceive("channel")->andReturn($channel);

        $factory = new RabbitMqFactory();
        $producer = $factory->createRpcClient($connection);
        $this->assertInstanceOf(RpcClient::class,$producer);
    }

}
