<?php
/**
 * @author Levin Mauritz <l.mauritz@posteo.de>
 *
 */

namespace PhpRabbitMq;

use PhpAmqpLib\Message\AMQPMessage;
use Mockery;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class PubSubConsumerTest extends TestCase
{

    public function testConsume()
    {
        $msg = new AMQPMessage();
        $msg->setBody("yolo");

        $callback = function (AMQPMessage $msg) {
            $GLOBALS["test"] = $msg->getBody();
        };

        $channel = $this->getMockChannel();
        $channel->shouldReceive("wait")->andReturn(call_user_func($callback, $msg));
        $connection = $this->getMockConnection($channel);

        $consumer = new PubSubConsumer(
            $connection,
            "exchange",
            "routingKey",
            "env",
            "queue",
            "consumerName"
        );

        $consumer->setCallback($callback);
        $consumer->consume(0);

        $this->assertEquals("yolo", $GLOBALS["test"]);
        $this->assertEquals("exchange", $consumer->getExchange());
        $this->assertEquals("routingKey", $consumer->getRoutingKey());
        $this->assertEquals("env", $consumer->getEnv());
        $this->assertEquals("queue", $consumer->getQueue());
        $this->assertEquals("consumerName", $consumer->getConsumerName());
    }

    public function testLogging()
    {
        $logger = Mockery::mock(LoggerInterface::class);
        $logger->shouldReceive("notice");
        $channel = $this->getMockChannel();
        $channel->shouldReceive("wait");
        $connection = $this->getMockConnection($channel);
        $consumer = new PubSubConsumer($connection, "test", "", "", "", "", $logger);
        $consumer->consume(0);
        $this->assertEquals("test", $consumer->getExchange());
    }

    private function getMockChannel()
    {
        $channel = Mockery::mock(AMQPChannel::class);
        $channel->shouldReceive("exchange_declare");
        $channel->shouldReceive("queue_declare");
        $channel->shouldReceive("queue_bind");
        $channel->shouldReceive("basic_consume");
        $channel->shouldReceive("callbacks")->once()->andReturn(1);
        $channel->shouldReceive("close");
        return $channel;
    }

    private function getMockConnection($channel)
    {
        $connection = Mockery::mock(AMQPStreamConnection::class);
        $connection->shouldReceive("channel")->andReturn($channel);
        $connection->shouldReceive("close");
        return $connection;
    }
}
