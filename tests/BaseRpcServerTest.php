<?php
/**
 * @author Levin Mauritz <l.mauritz@posteo.de>
 *
 */

namespace PhpRabbitMq;

use PhpRabbitMq\BaseRpcServer;
use function call_user_func;
use PhpAmqpLib\Message\AMQPMessage;
use PhpRabbitMq\BaseConsumer;
use Mockery;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class BaseRpcServerTest extends TestCase
{

    public function testConsume()
    {
        $msg = "yolo";

        $callback = function ($msg) {
            $GLOBALS["test"] = $msg;
            return "got: " . $msg;
        };

        $channel = $this->getMockChannel();
        $channel->shouldReceive("wait")->andReturn(call_user_func($callback, $msg));
        $connection = $this->getMockConnection($channel);

        $consumer = new BaseRpcServer(
            $connection,
            new RabbitMqFactory(),
            "exchange",
            "routingKey",
            "env",
            "queue",
            "consumerName"
        );

        $consumer->setCallback($callback);
        $consumer->start();

        $this->assertEquals("yolo", $GLOBALS["test"]);
        $this->assertEquals("exchange", $consumer->getExchange());
        $this->assertEquals("routingKey", $consumer->getRoutingKey());
        $this->assertEquals("env", $consumer->getEnv());
        $this->assertEquals("queue", $consumer->getQueue());
        $this->assertEquals("consumerName", $consumer->getConsumerName());
    }

    private function getMockChannel()
    {
        $channel = Mockery::mock(AMQPChannel::class);
        $channel->shouldReceive("exchange_declare");
        $channel->shouldReceive("queue_declare");
        $channel->shouldReceive("queue_bind");
        $channel->shouldReceive("basic_consume");
        $channel->shouldReceive("callbacks")->once()->andReturn(1);
        $channel->shouldReceive("close");
        return $channel;
    }

    private function getMockConnection($channel)
    {
        $connection = Mockery::mock(AMQPStreamConnection::class);
        $connection->shouldReceive("channel")->andReturn($channel);
        $connection->shouldReceive("close");
        return $connection;
    }
}
