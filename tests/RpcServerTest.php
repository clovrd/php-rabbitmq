<?php
/**
 * @author Levin Mauritz <l.mauritz@delinero.de>
 *
 */

namespace PhpRabbitMq;

use Mockery;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PHPUnit\Framework\TestCase;
use Reflection;
use ReflectionClass;

class RpcServerTest extends TestCase
{
    public function testInitServer() {
        $channel = Mockery::mock(AMQPChannel::class);
        $connection = Mockery::mock(AMQPStreamConnection::class);
        $connection->shouldReceive("channel")->andReturn($channel);

        $serverName = "rpcServer";
        $server = new RpcServer($connection);
        $server->initServer($serverName);

        $this->assertEquals($serverName,$server->getExchangeOptions()["name"]);
        $this->assertEquals($serverName,$server->getQueueOptions()["name"]);
    }
}
